﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EarlyBirdLib
{
    public class StoreListing : INotifyPropertyChanged
    {
        private string displayName;
        private string description;
        private StoreApp listingApp;

        public int Id { get; set; }
        public string DisplayName { get => displayName; set { displayName = value; NotifyPropertyChanged(); } }
        public string Description { get => description; set { description = value; NotifyPropertyChanged(); } }
        public StoreApp ListingApp { get => listingApp; set { listingApp = value; NotifyPropertyChanged(); } }
        public int StoreAppId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
