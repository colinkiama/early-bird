﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace EarlyBirdLib
{
    public class StoreApp : Notifier
    {
        private string name;
        public List<StoreListing> StoreListings { get; private set; } = new List<StoreListing>();
        public int Id { get; set; }
        public string Name
        {
            get => name;
            set
            {
                name = value;
                NotifyPropertyChanged();
            }
        }
    }
}
