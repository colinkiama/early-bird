﻿using EarlyBird.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace EarlyBird.Core
{
    public static class DatabaseOptionsBuilders
    {
        internal static DbContextOptionsBuilder<StoreAppContext> StoreAppContextOptionsBuilder;
        static DatabaseOptionsBuilders()
        {
            var dbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, StoreAppContext.DBFileName);
            StoreAppContextOptionsBuilder = new DbContextOptionsBuilder<StoreAppContext>();
            StoreAppContextOptionsBuilder.UseSqlite($"Data Source={dbPath}");
            
        }
    }
}
