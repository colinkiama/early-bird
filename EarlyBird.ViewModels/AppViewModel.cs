﻿using EarlyBird.DbContexts;
using EarlyBirdLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EarlyBird.ViewModels
{
    public class AppViewModel : Notifier
    {
        public ObservableCollection<StoreApp> AppsToDisplay { get; set; } = new ObservableCollection<StoreApp>();
        public AppViewModel()
        {
            // Use dependecy injection to get 
            // db context settings specific to the client
            // application
            // Get list of apps from database
            var db = new StoreAppContext();
            
            //AppsToDisplay = .
            // Give value to AppsToDisplay.
        }
    }
}
