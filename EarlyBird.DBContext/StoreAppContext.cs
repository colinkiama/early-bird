﻿using EarlyBirdLib;
using Microsoft.EntityFrameworkCore;
using System;

namespace EarlyBird.DbContexts
{
    public class StoreAppContext : DbContext
    {
        public const string DBFileName = "storeApps.db";
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                options.UseSqlite($"Data Source={DBFileName}");
            }

        }

        public DbSet<StoreApp> StoreApps { get; set; }
        public DbSet<StoreListing> StoreListings { get; set; }

        public StoreAppContext() : base() { }
        public StoreAppContext(DbContextOptions<StoreAppContext> options) : base(options) { }
    }
}
