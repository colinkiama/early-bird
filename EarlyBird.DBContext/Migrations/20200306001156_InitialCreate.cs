﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EarlyBird.DbContexts.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StoreApps",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreApps", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StoreListings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DisplayName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StoreAppId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreListings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StoreListings_StoreApps_StoreAppId",
                        column: x => x.StoreAppId,
                        principalTable: "StoreApps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StoreListings_StoreAppId",
                table: "StoreListings",
                column: "StoreAppId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StoreListings");

            migrationBuilder.DropTable(
                name: "StoreApps");
        }
    }
}
